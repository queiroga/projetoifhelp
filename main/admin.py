from django.contrib import admin
from .models import ProjetoIntegrador,Categoria,Curso,Professor

admin.site.register(ProjetoIntegrador)
admin.site.register(Categoria)
admin.site.register(Curso)
admin.site.register(Professor)