from rest_framework.viewsets import ModelViewSet
from main.api.serializers import ProjetoIntegradorSerializer,ProfessorSerializer
from main.models import ProjetoIntegrador,Professor



class ProfessorViewSet(ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Professor.objects.all()
    serializer_class = ProfessorSerializer

class ProjetoViewSet(ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = ProjetoIntegrador.objects.all()
    serializer_class = ProjetoIntegradorSerializer
