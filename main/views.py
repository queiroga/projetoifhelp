from django.shortcuts import render,redirect
from .forms import ProjectForm
from django.views.generic.list import ListView
from .models import ProjetoIntegrador,Categoria

# Create your views here.

def index(request):
    return render(request, 'index.html')

def newproject(request):
    form = ProjectForm
    return render(request, 'main/form.html', {'form': form})

def createProject(request):

    if request.method == "POST":

        form = ProjectForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect('list_project')
    else:
        form = ProjectForm()

    render(request,'main/form.html',{'form': form})


def listProject(request):
    return render(request,'main/list.html')



class ProjectListView(ListView):
    template_name = "main/list.html"
    model = ProjetoIntegrador
    paginate_by = 100  # if pagination is desired

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context