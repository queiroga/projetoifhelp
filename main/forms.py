from django import forms
from .models import ProjetoIntegrador,Categoria


class ProjectForm(forms.ModelForm):
    nome =forms.CharField(label='Nome:',
                       max_length=100,
                       widget=forms.TextInput(attrs={'class': 'form-control'}))
    categoria = forms.ModelChoiceField(queryset=Categoria.objects.all(), widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = ProjetoIntegrador
        fields = ('__all__')




