"""projetoIFHelp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from main import urls as urls_main
from rest_framework import  routers
from main.api.viewset import ProjetoViewSet,ProfessorViewSet

router = routers.DefaultRouter()
router.register(r'api/projetos', ProjetoViewSet)
router.register(r'api/professor', ProfessorViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(urls_main)),
    path('', include(router.urls)),
]
