from django.db import models
import datetime
from django.utils.encoding import python_2_unicode_compatible


class Curso(models.Model):
    nome = models.CharField(max_length=50)

    def __str__(self):
        return self.nome
    class Meta:
        verbose_name = "Curso"


class Categoria(models.Model):
    nome = models.CharField(max_length=50)

    def __str__(self):
        return self.nome
    class Meta:
        verbose_name = "Categoria"

class Professor(models.Model):
    nome = models.CharField(max_length=70)
    matricula = models.CharField(max_length=50)
    email = models.CharField(max_length=50)

    def __str__(self):
        return self.nome
    class Meta:
        verbose_name = "Professor"


class ProjetoIntegrador(models.Model):
    nome = models.CharField(max_length=70)
    descricao = models.TextField(verbose_name='Descrição', null=True, blank=True)
    date = models.DateField(("Date de Cadastro"), default=datetime.datetime.now())
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE, verbose_name='Categoria',null=True)
    curso = models.ForeignKey(Curso, on_delete=models.CASCADE, verbose_name='Curso', null=True)
    professores = models.ManyToManyField(Professor)
    ano = models.IntegerField()
    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = "Projeto Integrador"
        verbose_name_plural = "Projetos Integradores"


