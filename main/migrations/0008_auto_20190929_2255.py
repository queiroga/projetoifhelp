# Generated by Django 2.2.5 on 2019-09-30 01:55

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0007_auto_20190929_2253'),
    ]

    operations = [
        migrations.AlterField(
            model_name='projetointegrador',
            name='date',
            field=models.DateField(default=datetime.datetime(2019, 9, 29, 22, 55, 35, 205751), verbose_name='Date de Cadastro'),
        ),
    ]
