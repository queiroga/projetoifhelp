# Generated by Django 2.2.5 on 2019-09-30 01:53

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_auto_20190929_2251'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='projetointegrador',
            name='categoria',
        ),
        migrations.AlterField(
            model_name='projetointegrador',
            name='date',
            field=models.DateField(default=datetime.datetime(2019, 9, 29, 22, 53, 18, 108957), verbose_name='Date de Cadastro'),
        ),
    ]
