from rest_framework.serializers import ModelSerializer
from main.models import ProjetoIntegrador,Professor


class ProfessorSerializer(ModelSerializer):
    class Meta:
        model = Professor
        fields = "__all__";


class ProjetoIntegradorSerializer(ModelSerializer):

    professores = ProfessorSerializer(many=True)

    class Meta:
        model = ProjetoIntegrador
        fields = "__all__";