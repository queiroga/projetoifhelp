from hmac import new

from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('newproject/', newproject, name="newproject"),
    path('createProject/', createProject, name="createProject"),
    path('list_project/', ProjectListView.as_view(), name="list_project"),
]